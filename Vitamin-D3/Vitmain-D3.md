# Informationen zu Vitamin D3

## Inhalt

[[_TOC_]]


## Benützte Quellen
- Q1 [Wikipedia, Hypervitaminose D](https://de.wikipedia.org/wiki/Hypervitaminose_D)
- Q2 [Vitamin D - Eine Bestandesaufnahme](https://www.yumpu.com/de/document/read/21270168/vitamin-d-eine-bestandesaufnahme)
- Q3 [Eine neue Sicht auf Vitamin-D-Mangel, Fettleber und Übergewicht](https://www.healthandscience.eu/index.php?option=com_content&view=article&id=2382:eine-neue-sicht-auf-vitamin-d-mangel-fettleber-und-uebergewicht&catid=20&lang=de&Itemid=316)
- Q4 [Vitamin D Wirkung](https://www.vitamind.net/wirkung/)
- Q5 [Bereits 2011: Higher Vitamin D Intake Needed to Reduce Cancer Risk](https://health.ucsd.edu/news/2011/Pages/02-22-vitamin-D-cancer-risk.aspx)
- Q6 **Ein *sehr* lesenswerter Artikel:** [Vitamin D: Viel spricht für die tägliche und ausreichend hohe Substitution](http://www.hevert.com/market-de/arzt/de/meine_praxis/medizin_aktuell/artikel/vitamin-d-taegliche-und-ausreichend-hohe-substitution)
- Q7 [Vitamin D und Vitamin K](https://www.vitamind.net/vitamin-k/)
- Q8: Berkner, K. L. and Runge, K. W. (2004), The physiology of vitamin K nutriture and vitamin K-dependent protein function in atherosclerosis. Journal of Thrombosis and Haemostasis, 2: 2118–2132. doi: 10.1111/j.1538-7836.2004.00968.x"
- Q9: Masterjohn C. Vitamin D toxicity redefined: vitamin K and the molecular mechanism. Med Hypotheses. 2007;68(5):1026-34. Epub 2006 Dec 4. PubMed PMID: 17145139.
- Q10: D. Feskanich, P. Weber, W. C. Willett, H. Rockett, S. L. Booth, G. A. Colditz: Vitamin K intake and hip fractures in women: a prospective study. In: The American journal of clinical nutrition. Band 69, Nummer 1, Januar 1999, S. 74–79, ISSN 0002-9165. PMID 9925126.
- Q11: [Fünf Störfaktoren für die körpereigene Vitamin-D-Bildung](https://www.zentrum-der-gesundheit.de/artikel/vitamin-d/vitamin-d-bildung-stoerung-ia)
- Q12 [Vitamin D-Versorgung kann Sterberisiko bei Covid-19 mitbestimmen](https://www.lungenaerzte-im-netz.de/news-archiv/meldung/article/vitamin-d-versorgung-kann-sterberisiko-bei-covid-19-mitbestimmen/)
- Q13 [Ab in die Sonne – Was wir uns von Naturvölkern abschauen können!
](https://zsmed.de/ab-in-die-sonne-was-wir-uns-von-naturvolkern-abschauen-konnen/)
- Q14 Krebsprävention mit Vitamin D: [Vitamin D Supplement Doses and Serum 25-Hydroxyvitamin D in the Range Associated with Cancer Prevention](http://ar.iiarjournals.org/content/31/2/607.full.pdf+html%20(free%20fulltext))
- Q15 Vitamin D reduziert Atemwegsinfektionen: [Vitamin D reduces respiratory infections](https://www.sciencedaily.com/releases/2016/11/161116103005.htm)
- Q16: Vitamin D senkt das Risiko für akute Atemwegsinfekte um bis zu 70 Prozent: [Der optimale Vitamin-D-Spiegel - was wir von Naturvölkern lernen können](https://www.pressetext.com/news/dr-jacobs-institut-der-optimale-vitamin-d-spiegel-was-wir-von-naturvoelkern-lernen-koennen.html)
- Q17: 
[Studie vom Grassroots Forschungsinstitut für Gesundheitsnährstoffe / Grassroots Health Nutrient Research Institute](https://www.grassrootshealth.net/document/disease-incidence-prevention-chart-in-ngml/)
- Q18: [7 jährige Studie: *Zusammenfassend scheint eine langfristige Supplementation mit Vitamin D3 in Dosen von 5000 bis 50.000 IU/Tag sicher zu sein*](https://www.sciencedirect.com/science/article/abs/pii/S0960076018306228?via%3Dihub)


# Einleitung: Lohnt es sich, über Vitamin D3 nachzudenken?

Q17: Obwohl schon vor mind. 12 Jahren (2008) medizinische Studien gezeigt haben, dass Vitamin D3 das Risiko massiv senken kann, an schweren Krankheiten zu erkranken, haben sich viele Ärzte leider immer noch nicht schlau gemacht. Hier ein Beispiel, warum es sich lohnt, den Einfluss von Vitamin D3 auf die Gesundheit zu studieren: 

**Folgendes Diagramm zeigt, dass wenn der Blutspiegel vom in den USA üblichen Durchschnitt 25 ng/ml:**
- auf 34 ng/ml erhöht wird, dann sinkt das Erkrankungsrisiko bei Brustkrebs um fast 1/3!
- auf 50 ng/ml erhöht wird, dann sinkt das Erkrankungsrisiko bei Brustkrebs um 83%!


**Erklärung der folgenden Grafik:**
- **(1)** In dieser Zeile wird das Brustkrebsrisiko in Abhängigkeit des Vitamin D3-Blutspiegels dargestellt.
- **(2)** Zeigt, dass Menschen ein 30% tieferes Risiko haben, an Brustkrebs zu erkranken, wenn ihr **(3)** Blutspiegel bei 34 ng/ml liegt
- **(4)** Zeigt, dass Menschen ein 83% tieferes Risiko haben, an Brustkrebs zu erkranken, wenn ihr **(4)** Blutspiegel bei 50 ng/ml liegt
- **(6)** Zeigt, dass durchschnittliche Bürger der USA nur einen Blutspiegel von ~25 ng/ml haben,
- **(7)** während Arbeiter, die draussen arbeiten, im Spätsommer einen Blutspiegel von ~50 ng/ml haben.


[![Beispiel: Brustkrebs-Risiko in abhängigkeit des Vitamin D3-Blutspiegels](https://gitlab.com/jig-opensource/medizin/-/raw/master/Vitamin-D3/GrassrootsHealth%20-%20Disease%20incidence%20prevention%20Chart/Disease%20incidence%20prevention%20Chart%20-%20GrassrootsHealth%20-%20Beispiel%20Brustkrebs.png)](https://www.grassrootshealth.net/document/disease-incidence-prevention-chart-in-ngml/)

Unsere Intuition zeigt, dass Menschen, die oft draussen in der Natur sind, ziemlich sicher gesünder leben. Beim Vitamin D3 weist es die Naturwissenschaft sogar nach :-)

Das Original-Diagramm findet sich [hier (Q17)](https://www.grassrootshealth.net/document/disease-incidence-prevention-chart-in-ngml/)



# Das Problem #1: Die Pharmaindustrie hasst günstige, natürliche Mittel, die vor schweren Krankheiten schützen
Mit einem Abonnement z.B. bei der New York Times kann man herausfinden, dass die Pharmaindustrie schon zur Zeit des zweiten Weltkriegs erkannte, dass Vitamin D3 gegen etliche, teilweise sehr schwere Krankheiten schützt. Deshalb versuchte die Pharmaindustrie regelmässig zu erreichen, dass Vitamin D3 nur als Medikament verkauft werden darf. Glücklicherweise forderte damals der US-Senat, dass sie mit den vielen Anträgen endlich aufhören. Vitamin D3 bleibe ein Nahrungsergänzungsmittel.

Die Lösung der Pharmaindustrie ist perfid und hält auch heute noch an:
So bietet Beipsielsweise Sandoz das Produkt "Vitamin D3 Sandoz Tabl 1000 IE" an. Obwohl Sandoz die Forschung kennt, dosieren sie es um Faktoren zu tief: damit das Vitamin D3 seine Schutzfunktion gegen Krebs und Multiple Sklerose entfalten kann, müsste ein Patient pro Tag 8 Tabletten nehmen, was einem Menschen natürlich widerstrebt und Zweifel streut, ob das gesund sein kann.


# Das Problem #2: Die Ärzte machen sich nicht mehr selber schlau

In der Schweiz haben sich Ärzte zum Beipsiel im "innovativen Ärzte-Netzwerk" [mediX](https://www.medix.ch) organisiert. Der meiX Blog schreibt über sich selber: [**Qualität ist bei mediX mehr als ein Schlagwort**](https://medixblog.ch/ueber-medix/was-ist-eigentlich-medix-und-wer-steht-dahinter/)

Wenn wir die Qualität am Beispiel vom Vitmin D3 prüfen, können wir lesen, was sie über den [Vitamin D-Mangel](https://www.medix.ch/wissen/guidelines/stoffwechselkrankheiten/vitamin-d-mangel.html) schreiben:

**Indiz #1 zur völlig ungenügenden mediX-Qualität: "Zuletzt revidiert: 09/2016"**

mediX hat sich also entschieden, seit 4 Jahren die Forschung zu ignorieren, obwohl:
- die Forschung im Bereich Vitamin D3 sehr aktiv ist
- die Resultate für Patienten sehr wichtig sind, weil schwerste Krankheiten im Fokus sind
- die Resultate auch fürs Gesundheitswesen zentral sind, weil unverschämt hohe Kosten zur Heilung der schwersten Krankheit vermieden werden können

Eine Mission zum Wohl der Patienten sähe anders aus.

**Indiz #2 zur völlig ungenügenden mediX-Qualität: Bereits 2008 bekannte Forschungsresultate werden schon mehr als 1 Jahrzent ignoriert**

Schon im Jahr 2008 waren die Resultate von Beispielsweise der Studie Q17 bekannt, die zeigt, dass Blutspiegel benötigt werden, wie sie bei Naturvölkern gefunden werden, damit das Vitamin D3 seine Schutzfunktion entfalten kann. 

Trotzdem empfiehlt mediX zum [Vitamin D-Mangel](https://www.medix.ch/wissen/guidelines/stoffwechselkrankheiten/vitamin-d-mangel.html):

> 50 nmol/l (> 20 ng/ml): Dieser Blutspiegel stellt eine ausreichende Vitamin D3 Versorgung dar.

Wenn wir uns die Verhältnisse vor Augen führen, so stellt sich die Frage, warum mediX empfiehlt, dass ein 1.5fach tieferer Wert genügt, denn nur schon der schlichte, gesunde Menschenvestand impliziert ein anderes Bild:

```
                         Natürlicher Blutspiegel
                         bei Naturvölkern
                              │
                              │         220 nmol/l (88 ng/ml)
  25 nmol/l (10 ng/ml)        │         Empfohlener Maximalwert 
  │                           │         │
  ▼                      ┌────┴────┐    ▼
  ───────────────────────▓▓▓▓▓▓▓▓▓▓▓──────────► Blutspiegel Vitamin D3
           ▲              ▲  ▲
           │              │  │
           │              │  150 nmol/l
           │              │  Studie von GrassrootsHealth:
           │              │  Mit diesem Blutspiegel kann Vitamin D3 sein Potential entfalten,
           │              │  um gegen viele schwere Krankheiten zu schützen.
           │              │
           │              123 nmol/l:
           │              Normaler Blutspiegel,
           │              wenn ein Mensch täglich in der Sonne ist
           │
           50 nmol/l
           mexiX:
           "Der Vitamin D-Spiegel ist bei 50 nmol/l ausreichend"
```



# Die Auseinandersetzung mit dem Hausarzt
Die Auseinandersetzung mit dem Hausarzt ist leider oft schwierig, weil er entweder die aktuelle Forschung nicht kennt oder weil er sich oft keine Rechenschaft über einfache, logische Fragen ablegt. Diese Fragen helfen:

## Beweise? Ganz einfach: Was erfahren wir in der Schöpfung über Vitamin D3?
Obwohl es der Evolution völlig entgegen steht, erwarten wir *nie* Zufälle, wenn wir die Schöpfung untersuchen, weil wir wissen, dass wir immerzu Regeln entdecken.

## Zwei sehr hilfreiche Fragen - nicht nur an Ärzte

### Frage 1: Welchen Vitamin D3 Blutspiegel haben 'Naturvölker'?
Diese Frage ist sehr spannend, weil wir erwarten können, dass das Resultat ziemlich sicher den gesunden Mittelwert des Blutspiegels widerspiegelt, wenn sich Menschen wie gedacht normal / häufig in der Natur bewegt.

Q13 berichtet, dass die beiden Naturvölker der Massai und Hadzabe untersucht wurden: sie leben nahe des Äquators, halten sich überwiegend im Freien auf und ihre Körper sind nur mäßig mit Kleidung bedeckt. Direkte starke Sonneneinstrahlung meiden sie.

**Das Resultat des Vitamin D-Blutspiegel beider Völker:**
Durchschnittlicher Wert: 114 nmol/l (46 ng/ml) 
Das Maximum: ~170 nmol/l (68 ng/ml).

### Frage 2: Wie viel Vitamin D3 nimmt ein Mensch pro Tag auf, wenn er sich in seinem natürlichen Umfeld - also draussen - aufhält?
- Q11: Ein Mensch produziert im Bikini / in der Badehose bereits nach 30 Minuten in der Sonne etwa 10'000 IE Vitamin D3.
- Q1: Der Körper stoppt die Vitamin D3-Produktion durch das Sonnenlicht bei jungen Erwachsenen bei maximal 10'000-20'000 IE (250–500µg) täglich.

## Fazit: Pharmaindustrie vs Beobachtungen in der Schöpfung
Wenn also empfohlen wird, dass pro Tag 500 oder 1'000 IE Vitamin D3 genügen, 
dann geht die Empfehlung davon aus, dass der natürliche Mensch pro Tag zwischen 1.5 und 3 Minuten in der Sonne ist.
Das ist ein extrem unnatürliches und ungesundes menschliches Verhalten. Worin liegt die Motivation dieser Empfehlung? Warum sollte man ihr Glauben schenken? Weshalb gibt es keine Studien, die beweisen, dass diese Empfehlung sinnvoll ist?

## Was, wenn der Hausarzt sich querstellt?

Ein guter Hausarzt müsste in diesem Fall gute, transparente Forschungsresultate vorlegen können - wenn er für den Patienten das Beste sucht.

Vitamin D3 ist *nicht* rezeptpflichtig. Trotzdem muss man natürlich gewissenhaft damit umgehen, ich würde so vorgehen:
1. Den Hausarzt bitten, den Vitamin D Blutspiegel zu messen und das Resultat mitzuteilen. Man kann z.B. auch bei Viollier vorbei gehen, der Test kostet etwa 52.-
2. Dem Kapitel *Sinnvolle Einnahme-Empfehlung* folgen
3. Jedes Jahr den Blutspiegel kontrollieren


# Der Schutz von Vitamin D3 gegen schwere Krankheiten

In Q5 haben sie festgestellt (gekürzt und übersetzt mit [deepl.com](https://www.deepl.com/translator)):
> "Forscher haben berichtet, dass eine deutlich höhere Zufuhr von Vitamin D erforderlich ist, um Blutwerte zu erreichen, die die Inzidenz von Brustkrebs und mehreren anderen schweren Krankheiten verhindern oder deutlich verringern können, als ursprünglich angenommen."

> "Bei Erwachsenen ist die tägliche Zufuhr von 4'000-8'000 IE Vitamin D erforderlich, um die Blutspiegel von Vitamin-D-Metaboliten in dem Bereich zu halten, der erforderlich ist, um das Risiko verschiedener Krankheiten wie z.B. Brustkrebs, Darmkrebs, Multiple Sklerose und Typ-1-Diabetes um etwa die Hälfte(!) zu senken."

> "Jetzt ist es an der Zeit, dass praktisch jeder mehr Vitamin D einnimmt, um einigen schweren Krebsarten, mehreren anderen schweren Krankheiten und Knochenbrüchen vorzubeugen"

Q6 schreibt:
> "Mittlerweile bestätigten Tests und Interventionsstudien, dass Vitamin D3 vor Krebserkrankungen schützt und bei bestehenden Tumorerkrankungen das Tumorwachstum hemmt, indem es die Apoptose fördert, die Gefäßneubildung verringert und die Bildung von Rezidiven und Metastasen abschwächt."

> "Was die Studie von TCS Woo bereits nahelegte, wurde in der Studie von CF Garland et al. (2011) bestätigt. Für die Tumorprävention muss Vitamin D3 hochdosiert werden. In der Studie von CF Garland et al. war eine tägliche Einnahme von 4.000 bis 8.000 IE Vitamin D3 erforderlich und selbst Dosierungen bis zu 10.000 IE Vitamin D3 pro Tag lösten keine toxische Wirkung aus."

> *(Siehe auch Q14!)* "Aus früheren Studien war bekannt, **dass für eine Krebs-präventive Wirkung 25 (OH)-D-Serumspiegel von 60 bis 80 ng/ml (150-200 nmol/l) nötig seien.** Wie schwierig solche Werte zu erreichen sind, zeigte die Studie von CF Garland et al. ebenfalls: **Damit 97,5 Prozent der 3.667 Studienteilnehmer auch nur einen Vitamin-D3-Serumspiegel von 40 ng/ml erreichten, waren Dosierungen von 9.600 IE pro Tag notwendig.** Die Studienautoren gehen sogar davon aus, dass tägliche Gaben von bis zu 40.000 IE keine toxische Wirkung hätten."

# Die Sinnvolle Einnahme-Empfehlung
In den nachfolgenden Kapiteln sind die die Hintergründe zu dieser Einnahme-Empfehlung aufgelistet.
Weil inzwischen krankhaft ideologisch versucht wird, die Evolutions-Theorie als Naturwissenschaft darzustellen (es gibt keinen einzigen(!) naturwissenschaftlichen Beweis für sie, wohl aber endlos viele Indizien dagegen), setze ich ganz bewusst den Kontrapunkt:
1. **Ziel:** Q6: Der Vitamin D3 Blutspiegel bzw. der Serumspiegel von 25(OH)D sollte zwischen 80 und 100 ng/ml (200-250 nmol/l) sein, damit das Vitamin D3 ganz sicher so hoch ist, damit es die präventive Wirkung gegen schwere Krankheiten entfalten kann. Dieser Serumspiegel entspricht der Beobachtung der Schöpfung: Naturvölker haben im Durchschnitt diese Blutwerte.
2. **Kontrolle:** Man kann Vitamin D3 zu hoch dosieren. Deshalb z.B. jährlich den Serumspiegel prüfen lassen. Q14 weist nach, dass ein Blutspiegel von 80 ng/ml (200 nmol/l) nicht toxisch / giftig ist. Der Spiegel sollte aber nicht höher sein, was ganz gut zur Grössenordnung der untersuchten Naturvölker passt.
3. **Tägliche Einnahme!:** Die Forschung hat die Beobachtung der Schöpfung bestätigt: die tägliche Einnahme ist die einzig(!) sinnvolle Form, weil die Halbwertszeit von 25-OH-D nur bei 24h und Calcitriol [1,25 (OH)2D3] nur bei 2h liegt. Auch nach einer hochdosierten Einmalgabe steht nach wenigen Tagen nicht mehr ausreichend Vitamin D zur Verfügung.
4. **Die Menge:** 8'000-10'000 IE Vitamin D3 / Tag. Begründung: Wiederum bestätigt die Forschung, dass die Beobachtung der Schöpfung sehr gut passt: Der Körper stoppt die Produktion zwischen 10'000-20'000 IE Vitamin D3. Die Forschung fand heraus, dass erst 8'000 bis 10'000 IE Vitamin D3 dazu führen, dass der minimale Serumspiegel von ca. 80 ng/ml erreicht werden.
5. **Wichtig: Vitamin K2!** Nachfolgend wird in den Quellen Q7-Q10 darauf hingewiesen, dass man Vitamin D3 unbedingt mit Vitamin K2 kombinieren sollte.
6. **Das Produkt:** Ich würde kein esoterisches Produkt, sondern ein medizinisches Produkt wählen, weil diese sich gewissenhaft der Naturwissenschaft verpflichtet haben und die Qualität gesichert ist. Eine Produkt mit 10'000 IE wird tatschlich diese Menge haben. In der Schweiz gibt es z.B. ViDe 3 Tropfen, leider braucht man für 8'000 IE ganze 80 Tropfen. Weil man aber ohnehin das Vitamin K2 kombinieren sollte, lohnt es sich, gleich ein Kombi-Produkt zu wählen.

Somit ist Sandoz' Vitamin D3-Präparat mit 1'000 IE-Tabletten ein verwerfliches Produkt, denn gewiss kennen sie die Forschung - ignorieren sie aber. Oder erwartet Sandoz, dass man pro Tag 8-10 Tabletten schluckt?

# Allgemeine Fakten zu Vitamin D3
## Umrechnung der Masseinheiten des Blutspiegels
Um die Masseinheiten des Blutspiegels umzurechnen, hilft diese Internetseite:

1. Webseite öffnen: [Umrechnungstabelle Einheiten](https://www.synlab.de/vet/tieraerzte-und-fachkreise/service/umrechnungstabelle-einheiten)
2. Den **Bereich Vitamine** aufklappen
3. Bei **Vitamin D (25-OH)** entweder den Wert in ng/ml oder in nmol/l eintragen - der andere Wert wird automatisch berechnet


## Der Vitamin D3 Blutspiegel - Eigentlich: der Serumspiegel von 25(OH)D
- Q6: Der Vitamin D Blutspiegel muss für eine Krebs-präventive Wirkung 60 bis 80 ng/ml (150-200 nmol/l) sein. Also unbedingt z.B. Jährlich den Blutspiegel messen lassen!
- Q14 weist nach, dass ein Blutspiegel von 80 ng/ml (200 nmol/l) nicht giftig/toxisch ist. Höher sollte er aber nicht sein.

## Wichtig: Vitamin D3 unbedingt mit Vitamin K2 ergänzen!
- Q7: Bei der Einnahme von Vitamin D3 sollte man unbedingt auch das Vitamin K2 einnehmen.
- Q7: Sowohl zur Verwertung des Calciums als auch zur Aktivierung der gebildeten Proteine jedoch ist Vitamin D auf einen wichtigen Partner angewiesen: das Vitamin K2.
- Q8: Ohne Vitamin K2 bleiben diese Proteine inaktiv und das Calcium lagert sich als nutzlose und schädliche Schlacke im Körper ab: Verkalkungen bilden sich und schädigen Gefäße, Organe und Gewebe. Die Folge sind Nierensteine, Arteriosklerose und zahlreiche schwerwiegende Krankheiten bis hin zum Herzinfarkt.
- Q9: Da Vitamin K2 die durch Vitamin D gebildeten Proteine aktiviert, ist es möglich, dass bei steigendem Vitamin-D-Spiegel leicht ein relativer oder absoluter Vitamin-K2-Mangel auftreten kann: Es wird mehr Vitamin K zur Aktivierung verbraucht, so dass sich der Vitamin-K-Pool im Körper erschöpft und das wichtige Vitamin für andere Prozesse im Körper nicht mehr zur Verfügung steht.
- Q10: In diesem Zusammenhang konnte auch gezeigt werden, dass sonst hilfreiche, hohe Vitamin-D-Spiegel das Risiko von Knochenbrüchen sogar erhöhen, wenn gleichzeitig ein Mangel an Vitamin K besteht.

## Unbedingt beachten, wenn man Vitamin D3 einnimmt
- Q3: Es scheint, dass übergewichtige Menschen mehr Vitamin D als empfohlen benötigen.
- Q6: *"Der renommierte Vitamin D-Forscher Prof. Dr. BW Hollis erklärt die Überlegenheit der täglichen Vitamin D-Gabe gegenüber der wöchentlichen oder monatlichen Einnahme mit der parakrinen und autokrinen Verstoffwechselung von Vitamin D3. Aufgrund der geringen Halbwertszeit des Provitamins 25-OH-D von 24 Stunden und des Calcitriol [1,25 (OH)2D3] von zwei Stunden stehe dem Gewebe und den Zellen auch nach einer hochdosierten Einmalgabe nach wenigen Tagen nicht mehr ausreichend Vitamin D zur Verfügung."*

## Ist Vitamin D3 mit 10'000 IE pro Tag nicht gefährlich?
- Q1: Neueste Studien deuten darauf hin, dass zumindest bis 10.000 IU (250 µg) *täglich über sehr lange Zeiträume keine Nebenwirkungen zu erwarten sind.* Das passt zu den Forschungsresultaten, dass täglich 8'000 IE Vitamin D3 vor schweren Krankheiten schützen!
- Q18: 7 jährige Studie: *Zusammenfassend scheint eine langfristige Supplementation mit Vitamin D3 in Dosen von 5000 bis 50.000 IU/Tag sicher zu sein*
- Sicherheitshalber trotzdem z.B. jedes Jahr den Vitamin D3 Blutspiegel, bzw. den Serumspiegel von 25(OH)D prüfen lassen.


## Wo hilft Vitamin D3, bzw. welche Risiken können bei einem Mangel entstehen?
- Q3: Es ist seit langem bekannt, dass ein Mangel an Vitamin D das Risiko von Übergewicht erhöht.
- Es gibt Krankheiten, die die Vitamin D3 Produktion blockieren / reduzieren. Deshalb darf man nie vermuten, dass alles gut ist, nur weil man oft in der Sonne ist oder in einem sonnigen Land lebt - man kann trotzdem einen Vitamin D3-Mangel haben!
- Q4: Fast jede Zelle unseres Körpers verfügt über Vitamin-D-Rezeptoren. Vitamin D steuert auf diese direkte Weise über 2000 Gene und hat damit tiefgreifenden Einfluss auf die Funktion von Zellen, Organen und ganzen Systemen.

- Q12 berichtet, dass Vitamin D3 dem Körper hilft, sich gegen Grippe-Viren - also auch gegen Corona - zu wehren


Q6 berichtet, dass in Beobachtungsstudien Vitamin D3 als Hilfe für dem Körper als Schutz gegen diese Krankheiten in Zusammenhang gebracht wird:
- Arthritis / Rheumatische Polyarthritis
- Asthma
- Entzündliche Darmerkrankungen
- Depression » in diesem Zus'hang auch Vitamin B6 + B12 beachten!
- Diabetes
- Emphyseme
- Fibromyalgie
- Herzinsuffizienz
- Hypertonie
- Infekte / Infektionskrankheiten wie Grippe und Lungenentzündung
- Krebs: Verschiedene Karzinomarten, vor allem kolorektale Karzinome
- Kognitive Beeinträchtigungen
- Kardiovaskuläre Krankheiten: Koronararterienerkrankung
- Multiple Sklerose, vermutlich: allgemeiner Schutz gegen Autoimun-Krankheiten
- Muskelschwäche
- Parkinsonsche Krankheit
- Psoriasis
- Übergewicht
- Unfruchtbarkeit

- Q2 Erwähnt auch den Schutz gegen Tuberkulose

- Q15 und Q16 berichten, dass Vitamin D das Risiko für akute Atemwegsinfekte um bis zu 70 Prozent senkt


## Wie kann der Körper Vitamin D3 produzieren?
- Die Sonne muss höher als 45° des Horizonts stehen, damit der Menschliche Körper Vitamin D3 produzieren kann, **das heisst: nur dann, wenn auf einer flachen Ebene der Schatten, den man wirft, nicht länger als man selbst ist**. So zeigt z.B. das Sonnenstandsdiagramm von Biel, dass die Sonne nur ab ca. Anfang April bis ca. Mitte August oberhalb von 45° steht.
- Glas filtert UV-Licht. Im Auto, Büro oder unter einem Glasdach ist die Vitamin-D-Produktion folglich ungenügend.
- Q2: 80-90% des Vitamin D3 bildet der Körper über die Haut und das Sonnenlicht. Das heisst: Nahrung kann Vitamin D3 Mangel nicht beheben.
- Q2: Sonnencréme ab Schutzfaktor 30 hemmt die Vitamin D3 Produktion um >95%

## Andere Fakten
- Q1: Die Halbwertszeit liegt je nach Quelle zwischen 19 Tagen und 3-4 Monaten. Wenn man also 10'000 IE Vitamin D3 einnimmt, hat der Körper nach dieser langen Zeit noch die Hälfte. Somit ist die tägliche Einzeldosis weniger wichtig als der Blutspiegel.

# Umrechnungstabelle

Siehe auch obiges Kapitel **Umrechnung der Masseinheiten des Blutspiegels**

| Umrechnungstabelle | Tropfen VI-DE 3 | ml | μg/ml | IE | nmol/l |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Umrechnung für 1 Tropfen VI-DE 3 | 1 | 0.022 | 2.5 | 100 | 0.006 |
| Umrechnung für  1 ml | 45 | 1 | 112.5 | 4’500 | 0.281 |
| Umrechnung für 1 μg/ml | 0.400 | 0.009 | 1 | 40 | 0.003 |
| Umrechnung für 1 IE | 0.010 | 0.000 | 0.025 | 1 | 0.00006 |
| Umrechnung für 1 nmol/l | 810 | 3.556 | 400.0 | 16’000 | 1 |

# Meine persönliche Geschichte mit Vitamin D3

- Nachdem mein Hausarzt etwa 1.5 Jahre erfolglos nach der Ursache für meine ganz furchtbare Schlaflosigkeit gesucht hat, meinte er: 
*Wir können es noch mit Vitamin D3 probieren, Ihr Blutspiegel ist etwas knapp.*
- Er verordnete mir 500 IE Vitamin D3 pro Tag
- Ich rechnete kurz nach: Das enspricht der Menge, wie wenn ich pro Tag 90 Sekunden in der Sonne wäre. Das wäre ein offensichtlich **sehr** krankhaftes Verhalten. Weil meine Recherche zeigte, dass der natürliche Mensch pro Tag 10'000-20'000 IE Vitamin D3 produziert, wählte ich den tieferen Wert, also 10'000.
- So nahm ich also 10'000 IE Vitamin D3 pro Tag.
- Nach 7 Tagen war mein Schlaf fast wieder normal, jedoch mit 10-12h noch zu lang
- Nach etwa 2 Wochen war mein Schlaf wieder normal
- Ich dachte: *Mein Blutspiegel ist nun offenbar wieder i.O.*, also reduzierte ich die Dosis auf die 500 IE - wie vom Hausarzt dosiert.
- Nach wenigen Wochen hatte ich die Schlafstörungen wieder. Ich brauchte eine ganze Weile, um die zu tiefe Dosierung im Verdacht zu haben.
- Und so erhöhte ich die Dosis jede Woche um 1'000 IE und schaute, was passiert.
- Bei 6'000-7'000 IE wurde mein Schlaf wieder normal :-)
- Mit 10'000 IE/Tag erreichte ich die Serum-Obergrenze, weshalb ich vermutlich mit etwas weniger auskomme - ich wählte 8'000 IE/Tag und schaue, wie es sich verhält.


