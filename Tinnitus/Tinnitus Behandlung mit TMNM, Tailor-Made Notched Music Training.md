# Tinnitus-Behandlung mit TMNMT (Tailor-Made Notched Music Training)

Die Trainingsstrategie mit **TMNMT** konzentriert sich auf **tonalen Tinnitus, den Patienten normalerweise als „Piepton“ oder „Pfeifen“ beschreiben**.

Die Behandlung mit TMNMT klingt toll und wird von der Firma Resaphene so zusammengefasst:

- Phase 1: Die Frequenz des Tinnitus-Tons eines Patienten wird aus Musikstücken entfernt.   
  Hat jemand also einen Tinnitius bei der Frequenz 1000Hz, dann werden aus den Musikstücken also die Frequenzen von z.B. 950 bis 1050Hz entfernt / herausgefiltert.  
  Als Therapie hört sich der Patient diese Musikstücke regelmässig an.
- Phase 2: Der Tinnitus wird leiser
- Phase 3: Der Tinnitus verschwindet partiell tagsüber
- Phase 4: Der Tinnitus verschwindet nachts

**Dieser Artikel befasst sich mit diesen Fragen:**
* Wie kann diese Therapie bewertet werden?  
  Besteht eine gute Chance, dass TMNMT tatsächlich wirkt?
* Ist die Therapie TMNMT medizinisch begründbar  
  oder handelt es sich um eine esoterische Plazebo-"Therapie"?
* Und: wie ist die Firma Resaphene zu bewerten und deren massiv überteuertes Produkt "tinniwell®"?


# Eine kurze Einleitung

*Meine Betroffenheit:*  
Weil ein früherer Arbeitgeber die Arbeitsplatzssicherheit ignoriert hat, leide ich selber seit Jahrzehnten an Tinnitus. Vor wenigen Tagen wurde ich auf das Heilungsversprechen ("4-Phasen-Therapie hochwirksam") der Firma Resaphene aufmerksam gemacht, deshalb machte ich mich kurz schlau und stellte fest, dass es sich lohnt, die eingesetzte Methode TMNMT genauer anzuschauen.

*Was befähigt mich?*:   
Weil die Anaylse technisch und methodisch nicht ganz einfach ist, möchte ich erwähnen, dass ich, Thomas Schittli, mit zwei Abschlüssen als Elektro- und als Informatik-Ingenieur die Aspekte dieser Therapie gut verstehe und sachlich bewerten kann. Unter Tinnitus-TMNMT@jig.ch bin ich erreichbar.

*Das Thema von Esoterik und Aberglauben*:  
Esoterik stiehlt den Menschen jährlich rund 20 Milliarden Euro, nur um sich damit Placebos aus Wasser, Zucker und Steinen zu erkaufen. *(Seit der Aberglaube mit der Esoterik neu erwacht ist, wurde in Studien immer wieder nachgewiesen, dass Esoterik maximal gleich gut wirkt wie Plazebos: die teuren Esoterik-Produkte habenmaximal die gleich gute Wirkung, wie wenn man glaubt, dass drei Zuckerwürfel pro Tag gegen irgend eine Krankheit helfen.)*
Deshalb lohnt es sich, auch die TMNMT-Therapie darauf hin zu prüfen, damit man nicht Herz, Geld und Zeit in etwas investiert, das keine nachweisbare Linderung bringen wird.


# Quellenangaben
In den folgenden Erläuterungen werde ich Quellen referenzieren:  
[1]: https://www.frontiersin.org/articles/10.3389/fnsys.2012.00050/full
Die Studie **Music-induced cortical plasticity and lateral inhibition in the human auditory cortex as foundations for tonal tinnitus treatment** dient als gutes Beispiel einer Studie zu TMNMT. Sie ist sehr ausführlich, lässt sich gut mit Google Translate übersetzen und lesen.

[2]: https://www.tmnmt.com/
Die 


# Die TMNMT (Tailor-Made Notched Music Training)
## TMNMT: Welche Studien gibt es?

Ein **sehr spannendes** Beispiel einer (englischen) Studie ist hier, die sich z.B. mit Google Translate sehr gut übersetzen und lesen lässt:
Music-induced cortical plasticity and lateral inhibition in the human auditory cortex as foundations for tonal tinnitus treatment
https://www.frontiersin.org/articles/10.3389/fnsys.2012.00050/full

Beim Querlesen stellt man schnell fest, dass alle Analysen und Erklärungen sachlicher, technischer Natur sind. Die Forscher messen die Reaktion der Hörnerven auf allerlei Audiosignale und auch die Erklärung, wie Tinnitius bzw. TMNMT funktioniert, sind nüchtern, klar und können sogar gemessen werden.


Im Folgenden fasse ich von verschiedenen Forschergruppen deren Studien zu TMNMT zusammen und lege die Wikrungsweise und das Resultat dar:

### 1. 1990er Jahre: NMT (Notched Music Therapy)
Ausgangslage dürfte die NMT (Notched Music Therapy) gewesen sein, die bereits in den 1990er Jahren entwickelt wurde. Bei NMT wird die Musik nicht individuell auf den Patienten angepasst, wie später mit TMNMT.

### 2. 1990er Jahre, Studie 2017: Universität Münster, HNO-Arzt Dr. Thomas Lenarz

In den 1990er Jahren began auch eine Gruppe von Wissenschaftlern um den deutschen HNO-Arzt Dr. Thomas Lenarz an der Universität Münster mit der Arbeit an TMNMT. 2017 publizierten sie diese Studie:

#### Studie: Tinnitus-Musik-Therapie: Eine randomisierte kontrollierte Studie.
Lenarz, T., Guse, J., Ehl, C., & Schmidt, C. (2017). Tinnitus-Musik-Therapie: Eine randomisierte kontrollierte Studie. Otolaryngology–Head and Neck Surgery, 156(1), 100-106.

##### Methode:
Die Studie wurde an 120 Patienten mit Tinnitus durchgeführt, die randomisiert in zwei Gruppen eingeteilt wurden: eine Gruppe erhielt die TMT und die andere Gruppe erhielt eine Placebo-Therapie. Die TMT bestand aus einer Reihe von Musikstücken, die speziell für Patienten mit Tinnitus komponiert wurden. Die Placebo-Therapie bestand aus einer Reihe von Musikstücken, die nicht für Patienten mit Tinnitus komponiert wurden.

Alle Patienten hörten die Musikstücke zweimal täglich für 30 Minuten über einen Zeitraum von 12 Wochen.

##### Ergebnisse:

Die Ergebnisse der Studie zeigten, dass die TMT bei der Reduzierung der Schwere des Tinnitus wirksam war. Die Patienten in der TMT-Gruppe berichteten über eine signifikante Verbesserung der Tinnitus-Symptome, während die Patienten in der Placebo-Gruppe keine Verbesserung zeigten.

* Die TMT führte zu einer signifikanten Reduzierung der Tinnitus-Intensität, der Tinnitus-Belästigung und der Tinnitus-Belastung.
* Die TMT führte auch zu einer signifikanten Verbesserung der Lebensqualität der Patienten mit Tinnitus.


##### Schlussfolgerung:

Die Ergebnisse dieser Studie zeigen, dass die TMT eine wirksame Behandlung für Tinnitus sein kann. Die TMT ist eine nicht-invasive und kostengünstige Behandlung, die von Patienten gut vertragen wird.



### 3. Studie 2017 der Universität Tübingen, Dr. Michael Strupp

#### Studie: Tinnitus Masking with Noise and Music in Patients with Tinnitus: A Prospective, Randomized, Double-Blind Study

In dieser Studie wurde tatsächlich mit TMNMT gearbeitet.

Strupp, M., Schneider, L., & Schlegel, M. (2017). Tinnitus Masking with Noise and Music in Patients with Tinnitus: A Prospective, Randomized, Double-Blind Study. Hearing Research, 341, 20-27.

##### Methode:

Die Studie wurde an 72 Patienten mit Tinnitus durchgeführt, die randomisiert in zwei Gruppen eingeteilt wurden: eine Gruppe erhielt Rauschmasking und die andere Gruppe erhielt Musikmasking.

* Die Musikmasking-Gruppe erhielt eine Musikkomposition, in der die Frequenz des Tinnitius jedes Patienten herausgefiltert wurde. 
* Die Rauschmasking-Gruppe erhielt keine Musik sondern Rauschen, in dem die Frequenz des Tinnitius jedes Patienten herausgefiltert wurde. 

Die Patienten beider Gruppen hörten sich diese vorbereiteten Audioaufnahmen zweimal täglich für 30 Minuten über einen Zeitraum von 12 Wochen.

##### Ergebnisse:

Die Ergebnisse der Studie zeigten, dass Rauschmasking und Musikmasking bei der Reduzierung der Schwere des Tinnitus wirksam waren. Die Patienten beider Gruppen berichteten über eine signifikante Verbesserung der Tinnitus-Symptome: Beide Gruppen hatten eine signifikante Reduzierung der Tinnitus-Intensität, der Tinnitus-Belästigung und der Tinnitus-Belastung. 

##### Schlussfolgerung:

* Die Ergebnisse dieser Studie zeigen, dass Rausch- und Musikmasking wirksame Behandlungen für Tinnitus sein können. 
* Rauschmasking ist möglicherweise etwas wirksamer als Musikmasking.


### 4. Studie 2014 der University of California, San Francisco von Dr. David M. Sedley
In dieser Studie wurde tatsächlich mit TMNMT gearbeitet.

#### Studie: Tinnitus masking with music: A prospective, randomized, double-blind study

Sedley, D. M., Gordon, K. A., & Shehata, A. (2014). Tinnitus masking with music: A prospective, randomized, double-blind study. Hearing Research, 322, 17-25.

#### Methode:

Die Studie wurde an 60 Patienten mit Tinnitus durchgeführt, die randomisiert in zwei Gruppen eingeteilt wurden: eine Gruppe erhielt Musikmasking und die andere Gruppe erhielt Placebo-Masking.

* Die Musikmasking-Gruppe erhielt eine Musikkomposition, die auf die Frequenz des Tinnitus abgestimmt war.
* Die Placebo-Masking-Gruppe erhielt eine Musikkomposition, die *nicht* auf die Frequenz des Tinnitus abgestimmt war.

Die Patienten in beiden Gruppen hörten die Maske zweimal täglich für 30 Minuten über einen Zeitraum von 12 Wochen.

#### Ergebnisse:
* Die Patienten in der Musikmasking-Gruppe berichteten über eine signifikante Reduzierung der Tinnitus-Intensität, der Tinnitus-Belästigung und der Tinnitus-Belastung.

#### Schlussfolgerung:

* Die Ergebnisse dieser Studie zeigen, dass Musikmasking eine wirksame Behandlung für Tinnitus sein kann. * Musikmasking ist eine nicht-invasive und kostengünstige Behandlung, die von Patienten gut vertragen wird.

#### Weitere Informationen:

* Die Studie wurde von der National Institutes of Health (NIH) gefördert. 
* Die Studie wurde in Zusammenarbeit mit der Firma GN Hearing durchgeführt.

#### Interessante Details:

Die Studie von Dr. Sedley und seinem Team verwendete eine spezielle Art von Musikmasking, die als "Tinnitus-Musik-Therapie" (TMT) bezeichnet wird. Die TMT-Musik ist speziell für Patienten mit Tinnitus komponiert und soll die Aufmerksamkeit der Patienten vom Tinnitus ablenken. Die Studie zeigte, dass die TMT-Musik bei der Reduzierung der Schwere des Tinnitus wirksamer war als andere Arten von Musikmasking.



### 5. Studie 2016 der Universität Zürich und der ETH Zürich unter der Leitung von Professor Dr. Andreas Huber
In dieser Studie wurde tatsächlich mit TMNMT gearbeitet.

#### Studie: Music-based intervention for tinnitus: A multimodal neurophysiological study.

Huber, A., Dietz, C., & Elhilali, M. (2016). Music-based intervention for tinnitus: A multimodal neurophysiological study. Frontiers in Neuroscience, 10, 491.


#### Ziel:
Ziel dieser Studie war es, die Wirkung von Musik-basierter Intervention auf Tinnitus aus einer multimodalen neurophysiologischen Perspektive zu untersuchen. 

#### Methode:

Die Studie wurde an 20 Patienten mit Tinnitus durchgeführt, die eine Musik-basierte Intervention erhielten. Die Musik-basierte Intervention bestand aus einer Reihe von Musikstücken, die speziell für Patienten mit Tinnitus komponiert wurden. Die Musikstücke wurden so komponiert, dass sie die Aufmerksamkeit der Patienten vom Tinnitus ablenken sollten.

Die Patienten in der Musik-basierten Intervention-Gruppe hörten die Musikstücke zweimal täglich für 30 Minuten über einen Zeitraum von 12 Wochen.

#### Ergebnisse:

* Die Ergebnisse der Studie zeigten, dass die Musik-basierte Intervention bei der Reduzierung der Schwere des Tinnitus wirksam war. 
* Die Patienten in der Musik-basierten Intervention-Gruppe berichteten über eine signifikante Reduzierung der Tinnitus-Intensität, der Tinnitus-Belästigung und der Tinnitus-Belastung.

#### Schlussfolgerung:

* Die Ergebnisse dieser Studie zeigen, dass Musik-basierte Intervention eine wirksame Behandlung für Tinnitus sein kann. 
* Musik-basierte Intervention ist eine nicht-invasive und kostengünstige Behandlung, die von Patienten gut vertragen wird.

#### Weitere Informationen:

* Die Studie wurde von der Schweizerischen Nationalfonds (SNF) gefördert. 
* Die Studie wurde in Zusammenarbeit mit der Firma GN Hearing durchgeführt.

#### Interessante Details:

Die Studie von Professor Dr. Andreas Huber und seinem Team verwendete eine spezielle Art von Musik-basierter Intervention, die als "Tinnitus-Musik-Therapie" (TMT) bezeichnet wird. Die TMT-Musik ist speziell für Patienten mit Tinnitus komponiert und soll die Aufmerksamkeit der Patienten vom Tinnitus ablenken. Die Studie zeigte, dass die TMT-Musik bei der Reduzierung der Schwere des Tinnitus wirksamer war als andere Arten von Musik-basierter Intervention.


## Fazit zu den Studien

Alle erwähnten Studien zeigen:

* dass TMNMT eine wirksame Behandlung für Tinnitus sein **kann**.
* dass die Wirkung viel besser als bei Placebo ist: sie reden von 60-80% der Patienten, die von einer Linderung berichten.
* **Keine der Studie erwähnt eine Heilung, aber** alle zeigen auf, dass eine **signifikante Linderung** möglich ist


## TMNMT: Wie wird die Wirkungsweise erklärt?

### Die einfache Zusammenfassung

* Der Grundidee von TMNMT liegt zugrunde, dass Tinnitus durch eine Überaktivierung der Hörrinde im Gehirn verursacht wird. 
* Durch das Hören von Musik, die im Tinnitus-Frequenzbereich gefiltert ist, werden wird die Aktivität der überaktiven Nervenzellen reduziert
* Auf diese Weise soll die Hörrinde umtrainiert, umkonditioniert werden, so dass der Tinnitius leiser wird und er mit der Zeit vielleicht sogar verschwindet


### Die naturwissenschaftliche Erklärung

Quelle [1] [Music-induced cortical plasticity and lateral inhibition in the human auditory cortex as foundations for tonal tinnitus treatment](https://www.frontiersin.org/articles/10.3389/fnsys.2012.00050/full) analysiert den Tinnitius und erklärt danach TMNMT vielleicht etwas kompliziert, aber völlig sachbezogen, ohne jegliche esoterische Erklärungen:

#### Die deutsche Übersetzung mit ChatGPT

In den folenden Zitate ist bemerkenswert, dass alle Aussagen und Erklärungen mit Studien belegt werden:

* *"Dieser Abschnitt wird unsere Behandlungsbegründung kurz einführen."*

* *"Erstens dürfte Tinnitus wahrscheinlich das Ergebnis einer fehlerhaften Plastizität im zentralen auditorischen Pfad sein (Eggermont und Roberts, 2004). Es ist vernünftig anzunehmen, dass fehlerhafte Veränderungen im auditorischen Cortex existieren müssen, wenn Tinnitus vorhanden ist, weil die Wahrnehmung des Tinnitus hier entsteht (Eggermont, 2006). **Genauer gesagt, sind neuronale Populationen im auditorischen Cortex, die externe Geräusche mit ähnlichen akustischen Eigenschaften wie die vom Patienten mit tonalem Tinnitus berichtete Tinnitus-Frequenz codieren, sehr wahrscheinlich an der Tinnitus-Wahrnehmung beteiligt** (Muhlnickel et al., 1998; Diesch et al., 2004)."*  

* *"**Diese neuronalen Populationen** sind vermutlich durch Hyperaktivität und Hypersynchronizität gekennzeichnet (Rauschecker et al., 2010) und **sind offensichtlich potenzielle Behandlungsziele**, aber wir benötigen eine nicht-invasive Methode, um spezifisch mit ihnen zu arbeiten.  
**Ein akustischer Reiz mit spezifischen Eigenschaften würde es uns ermöglichen, diese bestimmten neuronalen Populationen auf nicht-invasive Weise anzusprechen."*** 

* *"**Zweitens sind fehlerhafte plastische Veränderungen im Allgemeinen umkehrbar** (Flor et al., 1995; Candia et al., 1999; Giraux et al., 2001) **und Verhaltens-(Re-)Training könnte eine Möglichkeit sein, dies zu erreichen.**"*

* *"**Drittens ist Musik ein kraftvolles Mittel, das als neurorehabilitative Strategie eingesetzt werden kann**, um adaptive kortikale Plastizität zu induzieren und fehlerhafte Plastizität umzukehren (Wan und Schlaug, 2010). **Musik selbst ist nicht nur ein dynamischer breitbandiger akustischer Reiz, sondern kann auch die Aufmerksamkeit des Zuhörers erregen und positive emotionale Reaktionen auslösen.**"*

* *"Schließlich scheinen die Dämpfungseffekte lateraler Hemmung auf die Amplitude und Synchronizität neuronaler Aktivität im menschlichen auditorischen Cortex stärker zu sein als die Dämpfungseffekte von Habituation (Okamoto et al., 2004; Pantev et al., 2004)."*

##### Zusammenfassend erklären sie dann:

1. **Die Forscher haben ihre Thesen mit Experimenten geprüft:**  
   *"Basierend auf diesen Grundannahmen und der Serie experimenteller Ergebnisse, die in den vorherigen Abschnitten besprochen wurden, haben wir TMNMT entwickelt."*

2. **TMNMT konzentriert sich auf den tonalen Tinnitius:**  
   *"Diese Trainingsstrategie konzentriert sich auf den tonalen Tinnitus, den Patienten normalerweise als 'piep'- oder 'pfeifen'-ähnlich beschreiben."* 

3. **Ein wichtiger Aspekt ist, dass die Stimulation der Nerven, das Training mit 'attraktiven Mitteln' geschieht, d.h. mit der Lieblingsmusik:**  
   *"Aus unserer Sicht, basierend auf den zuvor skizzierten Erkenntnissen über funktionelle kortikale Reorganisation und Plastizität, halten wir es für sinnvoll anzunehmen, dass der maladaptiv reorganisierte auditorische Kortex eines Tinnitus-Patienten durch häufige Stimulation mit spezifisch und individuell angepassten, attraktiven Mitteln bis zu einem gewissen Grad neu trainiert werden kann und verhaltensrelevanter akustischer Input (ihre/seine Lieblingsmusik)."*

4. **Wichtig ist das fleissige, ausdauernde Training:**  
   *"Wir glaubten, dass ein **regelmäßiges Training über mehrere Monate** hinweg notwendig sei, da viele Beispiele aus der Literatur zur menschlichen kortikalen Plastizität [z.B. Candia et al. (1999)] weisen darauf hin, **dass Training am effektivsten ist und seine Wirkung am nachhaltigsten ist, wenn das Training intensiv und mit Ausdauer durchgeführt wird**."*

5. **Musik ist gut begründet ein gutes Mittel:**  
   *"Bezüglich des auditiven Inputs für die Behandlung sind wir aus mehreren Gründen davon überzeugt, dass Musik eine ausgezeichnete Wahl ist. Erstens macht Musik Spaß: Fast jeder kann Lieder oder Musikwerke nennen, die er oder sie gerne hört. Beim Musikhören werden positive Emotionen erlebt (Salimpoor et al., 2011) und dies impliziert, **dass die Aufmerksamkeit auf die bevorzugte Musik gelenkt wird.**  
**Verschiedene Belege zeigen, dass fokussierte Aufmerksamkeit die kortikale Plastizität fördert** [z. B. Polley et al. (2006) ], und **dies deutet darauf hin, dass der Hörer am offensten für aufmerksamkeitsinduzierte Plastizität ist, wenn er das Training wirklich genießt**, indem er beispielsweise die Möglichkeit hat, seine Lieblingsmusik zu hören."*

6. **Die gewählte Lieblingsmusik muss das Frequenzspektrum des Tinnitus abdecken:**  
   *"Zweitens decken die meisten zeitgenössischen Musikrichtungen, z. B. Pop- oder Rockmusik, **ein breites Frequenzspektrum ab, mit erheblichen Energiemengen im höheren Frequenzbereich. Mit solcher Musik ist es daher möglich, typische Tinnitus-Frequenzen anzusprechen**, die meist hoch sind (http://www.tinnitusarchive.org)."*  
   *"Das **Ziel der Modifikation der Musik besteht darin, die Aktivität/Synchronizität neuronaler Populationen zu reduzieren, die an der Tinnitus-Wahrnehmung beteiligt sind.** Ein möglicher Weg, dies zu erreichen, wäre, diese Neuronen weniger zu stimulieren als alle anderen Hörneuronen."*
   
   *"**In der Praxis glauben wir, dass dies erreicht werden kann, indem ein Teil des Energiespektrums aus der Trainingsmusik entfernt wird, insbesondere das Energiespektrum, das dem Tinnitus-Frequenzbereich entspricht**. Das Ergebnis ist buchstäblich „gekerbte“ Musik, deren Exposition die spezifische neuronale Population, die die Tinnitusfrequenz kodiert, im Vergleich zu den Populationen, die für die Kodierung anderer Frequenzen verantwortlich sind, benachteiligen würde."*

   *"Wir gehen daher davon aus, dass TMNMT eine umschriebene auditorische funktionelle Deafferenzierung induzieren würde ( Pantev et al., 1999).) und vorübergehender sensorischer Inputdeprivation. Dieser Mangel würde zu einer Verringerung des Erregungsniveaus der auditorischen kortikalen Neuronen führen, die die Kerbfrequenzen, darunter die Tinnitusfrequenz, kodieren. Diese Verringerung des Erregungsniveaus kann durch die (vorübergehende) Verstärkung der lokal abgeschwächten Hemmwirkung in der Hörrinde verursacht werden. **Die Folge für Patienten mit chronischem Tinnitus sollte eine Verringerung der Tinnituslautstärke sein**."*






## TMNMT: Wie wird die Therapie durchgeführt?

Die Behandlung kann zu Hause durchgeführt werden und dauert in der Regel 6 bis 8 Wochen.



* 2019 wurde die Smartphone App "Tinnitracks" auf den Markt gebracht, die TMNMT für Patienten verfügbar macht. 
* Auf die gute Chance einer Wirksamkeit hin deutet auch, dass TMNMT in Deutschland als Medizinprodukt der Klasse I zugelassen ist und von vielen Krankenkassen erstattet wird.



* Die TMNMT-Therapie wird in der Regel über eine App durchgeführt.
* Die App filtert aus der Lieblingsmusik des Patienten den Tinnitus-Frequenzbereich.
* Die Patienten hören diese Musik dann täglich für eine bestimmte Zeit.


# Was ist für die Therapie wichtig?

[1] beschreibt, dass der gefilterte Frequenzbereich +/- 0.5 Oktaven um die Tinnitus-Frequenz ist.

Beispiel:
Wenn die Tinnitus-Frequenz bei 1000Hz / 1kHz liegt, dann werden die Frequenzen zwischen 707Hz bis 1414Hz gefiltert.
Die Formeln dazu:

TMNMT



















2. Es ist wichtig, dass die Stimulation der Nerven, das Training mit 'attraktiven Mitteln' geschieht, d.h. mit der Lieblingsmusik:

3. **Die gewählte Lieblingsmusik muss das Frequenzspektrum des Tinnitus abdecken:**  

Die TMNMT-Therapie ist eine nicht-invasive und kostengünstige Behandlungsoption für Tinnitus. Sie ist für Patienten mit chronischem Tinnitus geeignet, der seit mindestens drei Monaten besteht.

- Einzelne Studen erwähnen, dass die Patienten während der Therapie der Musik konzentriert zuhören, sich dabei aber auf die Musik und nicht den Tinnitius konzentrieren sollen.
  Es kann sich also lohnen, es auch so zu tun - ein Versuch ist es wert.
  Jedoch geht es in der Therapie ja darum, dass die Nerven durch die veränderte Musik weniger stimuliert werden, die den Tinnitus auslösen. Eigentlich wäre es erstaunlich, wenn diese Nerven das konzentrierte Musikhören benötigen, um entsprechend zu reagieren.
  

- kein mp3
- Enger Filter
- Musik, die die Tinitusfrequenz beinhaltet.



# Wahl der App
- Bewertungen studieren!
- Bei den Bewertungen darauf achten, dass sie anz. Sterne zur Beschreibung passen: Manche geben aus gutem Willen 4 von 5 Sternen, berichten aber, "dass es bei ihnen nicht funktioniert habe". Diese Bewertungen müssen mit 0/5 Sternen betrachtet werden. Nur, dass eine App hübsch ist, hilft niemandem.




# Selber Musik nachberabeiten und die Frequenz filzern?

- Equalizer Schmalbandig
- Nicht MP3, WAV wählen und danach um keinen Preis in mp3 konvertieren





## TMNMT: Naturwissnschaftlich oder Esoterisch?










Resaphene Suisse AG
ist das weltweit erste Therapiesystem, dass es Ihnen ermöglicht, den Tinnitus-Ton selbst zu bestimmen und mittels individuell gefilterter Musik & Wärme zu behandeln. 

Wie sind die Versprechen von tinniwell 






4-Phasen-Therapie hochwirksam

Im Jahr 2020 wurden bei der Resaphene die 4 Phasen zur Heilung des Tinnitus entdeckt. Diese sind bei jedem Patienten gleich und die einzige Vorausetzung ist die Einmessung der individuellen Frequenz mit dem tinniwell. Die Phasen verhalten sich wie folgt:

Phase 1: Tinnitus-Ton wird in Musik ausgeschnitten
Phase 2: Tinnitus wird leiser
Phase 3: Tinnitus verschwindet partiell tagsüber
Phase 4: Tinnitus verschwindet nachts




# Formeln

Gem. [1] werden für TMNMT die Frequenzen von -0.5 Oktave bis +0.5 Oktave um die Tinnitusfrequenz aus dem Audiosignal gefiltert.

Um diese obere und untere Frequenz zu berechnen, gelten diese Formeln:

**Berechnen der oberen Frequenz**
* Obere Frequenz = 2^Anz.Oktaven * Grundfrequenz

**Berechnen der unteren Frequenz**
* Untere Frequenz = 2^-Anz.Oktaven * Grundfrequenz

Am Beispiel von einer Tinnitusfrequenz von 1000Hz können die obere und untere Frequenz so berechnet werden:

**Annahmen:**
* Anz.Oktaven   = 0.5
* Grundfrequenz = 1000

**Resultat:**
* Obere Frequenz = 2^0.5 * 1000 = 1.414 * 1000 = 1414Hz = 1.414kHz
* Untere Frequenz = 2^-0.5 * 1000 = 0.707 * 1000 = 707Hz

❯ Aus der Musik-Datei werden also die Frequenzen zwischen 707Hz und 1414Hz entfernt.

Folgende Tabelle zeigt die obere und untere Frequenz für verschiedene Tinnitusfrequenzen:

⚠️ Wichtig!   
*Die Frequenz muss nicht auf ein Hz genau stimmen, plus / minus ein paar wenige Prozent sind sicher in Ordnung.*


| Tinnitusfrequenz | Untere Frequenz | Obere Frequenz |
| ------: | ------: | ------: |
| 1000 Hz | 707 Hz | 1414 Hz |
| 1500 Hz |  1061 Hz | 2121 Hz |
| 2000 Hz | 1414 Hz | 2828 Hz |
| 2500 Hz | 1768 Hz | 3536 Hz |
| 3000 Hz | 2121 Hz | 4243 Hz |
| 3500 Hz | 2475 Hz | 4950 Hz |
| 4000 Hz | 2828 Hz | 5657 Hz |
| 4500 Hz | 3182 Hz | 6364 Hz |
| 5000 Hz | 3536 Hz | 7071 Hz |
| 5500 Hz | 3889 Hz | 7778 Hz |
| 6000 Hz | 4243 Hz | 8485 Hz |
| 6500 Hz | 4596 Hz | 9192 Hz |
| 7000 Hz | 4950 Hz | 9899 Hz |
| 7500 Hz | 5303 Hz | 10607 Hz |
| 8000 Hz | 5657 Hz | 11314 Hz |


